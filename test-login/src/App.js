import './App.css';
import React from "react"
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./pages/home"
import Dashboard from "./pages/dashboard";
import Login from "./pages/login";
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <BrowserRouter>
          <Routes>
            <Route>
              <Route path="/" element={<Home />} />
              <Route path="/login" element={<Login />} />
              <Route path="/dashboard" element={<Dashboard />} />
            </Route>
          </Routes>
        </BrowserRouter>
      </header>
    </div>
  );
}

export default App;
