import React from 'react'

function home() {
    return (
        <>
            <div>
                <h1>Home page! any one can access</h1>
                <h2>Login to know more</h2>
                <a class="btn btn-sm btn-default" href="/" role="button">Home</a>
                <a class="btn btn-sm btn-default" href="/login" role="button">Login</a>
                <a class="btn btn-sm btn-default" href="/dashboard" role="button">Dashboard</a>
                
            </div>
        </>
    )
}

export default home
