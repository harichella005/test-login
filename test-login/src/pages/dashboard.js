import React from 'react'

function dashboard() {
    return (
        <>
            <div>
                <h1>Dashboard page! only for authenticated users<br></br> if not it will navigate to Login page!</h1>
            </div>
        </>
    )
}

export default dashboard
